# Fusion STmap Macros #

This is a collection of custom macros for Blackagic Fusion that let you generate STmap templates and apply STmap data to existing footage.

### How to install ###

To install, simply copy and paste the settings files to your local Fusion macros directory, then restart Fusion.

- Windows: %APPDATA%\BlackmagicDesign\Davinci Resolve\Fusion\Macros\
- macOS: TODO
- Linux: ~/.fusion/BlackmagicDesign/DaVinci\ Resolve/Fusion/Macros/

### How to use ###

#### STmapGenerator ####

This macro creates a default STmap image you can use in your tracking application to create distortion data for your shots. Simply add the macro to your scene, set the resolution of your shot, and export a single image with a Saver node. A compositor or technical lead might want do this once at the beginning of a project and save the images to a shared directory for everyone to access.

For a full example of this workflow, see [this post](https://openvisualfx.com/2020/05/04/stmap-lens-distort-workflow/) from Sean Kennedy over at OpenVisual FX.

#### ApplySTmap ####

This macro applies an existing STmap (that contains distortion data from your DCC) to a source plate. This can either be 3D renders or a backplate. To use, add the macro to your scene, attach the source footage to the yellow input and the STmap to the green input, and set the resolution settings to match the resolution of your comp. This last step is important, especially if you are using 3D renders with overscan.